package httpd

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"troquer.wms.api/helper"
	"troquer.wms.api/models"
)

func SyncWms(c *gin.Context) {
	params, _ := helper.ApiRequest(c)
	page := cast.ToInt(params["page"])
	totalRecords := cast.ToInt(params["totalRecords"])
	result := models.Repository(&models.SyncWmsService{}).Find(page, totalRecords)
	data := result["records"]
	total := cast.ToInt(result["totalRecords"])
	helper.SuccessRows(c, data, total)
}

func GenerateSyncWms(c *gin.Context) {
	params, _ := helper.ApiRequest(c)
	page := cast.ToInt(params["page"])
	records := cast.ToInt(params["records"])
	totalPages := cast.ToInt(params["totalPages"])
	lastPage := 0
	if page == 1 && totalPages != 0 {
		lastPage = totalPages
	} else if totalPages != 0 {
		lastPage = (totalPages + page) - 1
	}
	source := "api"
	models.SyncWmsService{}.SyncWmsAlmex(page, records, lastPage, source)
	var data map[string]interface{}
	helper.Success(c, data)
}
