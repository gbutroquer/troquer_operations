package httpd

import (
	"github.com/gin-gonic/gin"
	"troquer.wms.api/helper"
	"troquer.wms.api/models"
)

func StockManageBySku(c *gin.Context) {
	sku := c.Param("sku")
	data, err := models.StockManager{}.UpdateStockBySku(sku)
	if err != nil {
		helper.Error(c, err.Error())
	} else {
		helper.Success(c, data)
	}
}

func StockManage(c *gin.Context) {
	data := make(map[string]interface{})
	_, _ = models.StockManager{}.UpdateStock()
	helper.Success(c, data)
}
