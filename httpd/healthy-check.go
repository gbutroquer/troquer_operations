package httpd

import (
	"github.com/gin-gonic/gin"
	"time"
	"troquer.wms.api/helper"
)

type T struct {
	name string
}

func HealthyCheck(c *gin.Context) {
	var response map[string]interface{}
	response = make(map[string]interface{})
	response["time"] = time.Now()
	helper.Success(c, response)
}
