package httpd

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"troquer.wms.api/helper"
	"troquer.wms.api/models"
)

func GetProducts(c *gin.Context) {
	params, _ := helper.ApiRequest(c)
	page := cast.ToInt(params["page"])
	totalRecords := cast.ToInt(params["totalRecords"])
	result := models.Repository(&models.ProductsService{}).Find(page, totalRecords)
	data := result["records"]
	total := cast.ToInt(result["totalRecords"])
	helper.SuccessRows(c, data, total)
}

func GetProductBySku(c *gin.Context) {
	sku := c.Param("sku")
	result := models.ProductsService{}.GetBySku(sku)
	if result.ID == 0 {
		helper.Success(c, nil)
		return
	}
	helper.Success(c, result)
}

func SaveProduct(c *gin.Context) {
	params, _ := helper.ApiRequest(c)
	params["source"] = "manual"
	product := models.ProductsService{}.Factory(params)
	productIns, _ := models.ProductsService{}.Save(product)
	response := make(map[string]interface{})
	response["id"] = productIns.ID
	helper.Success(c, response)
}

func DeleteProduct(c *gin.Context) {
	sku := c.Param("sku")
	result, err := models.ProductsService{}.Delete(sku)
	if err != nil {
		helper.Error(c, err.Error())
		return
	}
	helper.Success(c, result)
}
