package models

import "time"

type SyncWms struct {
	ID            int    `json:"id,primary_key"`
	source        string `gorm:"size:10"`
	StartPage     int
	EndPage       int
	TotalPage     int
	TotalProducts int
	Success       bool
	SkuStock      string    `json:"sentStock" gorm:"default:N/A"`
	StartAt       time.Time `json:"createdAt" gorm:"autoCreateTime"`
	EndAt         time.Time
}
