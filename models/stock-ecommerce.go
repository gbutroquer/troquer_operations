package models

import (
	"encoding/json"
	"errors"
	"github.com/go-resty/resty/v2"
	"os"
	"strconv"
)

type NoStockUpdateProducts struct {
	NonExistent []string `json:"non_existent"`
	Sold        []string `json:"sold"`
}

type StockManager struct {
	skuList []map[string]interface{}
}

func (stock StockManager) UpdateStock() (skuList []map[string]interface{}, err error) {
	stock.CheckStockItems()
	for {
		stock.skuList = stock.GetStockItems()
		if len(stock.skuList) == 0 {
			return stock.skuList, nil
		}
		stock.AddEcommerceStock()
	}
}

func (stock StockManager) UpdateStockBySku(sku string) (res bool, err error) {
	prodSrv := ProductsService{}
	item := prodSrv.GetBySku(sku)
	if item.ID == 0 {
		errorMsg := "sku not found"
		return false, errors.New(errorMsg)
	}

	if item.StatusItem != "A" {
		errorMsg := "sku stock rules doesnt apply"
		return false, errors.New(errorMsg)
	}
	itemsList := stock.MagentoItemFormat(item)
	stock.skuList = append(stock.skuList, itemsList)
	stock.AddEcommerceStock()
	return true, err
}

func (stock StockManager) AddEcommerceStock() {
	var listUpdate []string
	stockList := make(map[string]interface{})
	if len(stock.skuList) == 0 {
		return
	}
	stockList["sourceItems"] = stock.skuList
	_, err := stock.SendRequest(stockList)
	if err == nil {
		for _, item := range stock.skuList {
			listUpdate = append(listUpdate, item["sku"].(string))
		}
		db.Model(Products{}).Where("sku in ?", listUpdate).Update("sent_stock", true)
	}
}

func (stock StockManager) GetNonStockUpdate(parameters map[string]interface{}) (response NoStockUpdateProducts, err error) {
	mgToken := MagentoAuth{}.GetToken()
	url := os.Getenv("MAGENTO_NON_STOCK")
	client := resty.New()
	resp, _ := client.R().SetAuthToken(mgToken).SetBody(parameters).Post(url)
	if resp.StatusCode() != 200 {
		respError := NoStockUpdateProducts{}
		errorMsg := resp.String()
		return respError, errors.New(errorMsg)
	}
	skusNoUpdate := NoStockUpdateProducts{}
	jsonInput, _ := strconv.Unquote(string(resp.Body()))
	_ = json.Unmarshal([]byte(jsonInput), &skusNoUpdate)
	return skusNoUpdate, nil
}

func (stock StockManager) SendRequest(parameters map[string]interface{}) (response bool, err error) {
	mgToken := MagentoAuth{}.GetToken()
	url := os.Getenv("MAGENTO_STOCK")
	client := resty.New()
	resp, _ := client.R().SetAuthToken(mgToken).SetBody(parameters).Post(url)
	if resp.StatusCode() != 200 {
		errorMsg := resp.String()
		return false, errors.New(errorMsg)
	}
	return true, nil
}

func (stock StockManager) CheckStockItems() {
	var rows []string
	var prodSrv = ProductsService{}
	rowsData, _ := db.Model(&prodSrv.product).Where("sent_stock = ?", false).Where("status_item = ?", "A").Rows()
	for rowsData.Next() {
		_ = db.ScanRows(rowsData, &prodSrv.product)
		rows = append(rows, prodSrv.product.Sku)
	}
	skusNoUpdate := NoStockUpdateProducts{}
	if len(rows) > 0 {
		skusNoUpdate, _ = stock.GetNonStockUpdate(stock.MagentoSkuFormat(rows))

		if len(skusNoUpdate.NonExistent) > 0 {
			db.Model(Products{}).Where("sku in ?", skusNoUpdate.NonExistent).Update("status_item", "N")
		}
		if len(skusNoUpdate.Sold) > 0 {
			db.Model(Products{}).Where("sku in ?", skusNoUpdate.Sold).Update("status_item", "S")
		}
	}
}

func (stock StockManager) GetStockItems() []map[string]interface{} {
	var rows []map[string]interface{}
	var prodSrv = ProductsService{}
	stockSize, _ := strconv.Atoi(os.Getenv("MAGENTO_STOCK_SIZE"))
	rowsData, _ := db.Model(&prodSrv.product).Where("sent_stock = ?", false).Where("status_item = ?", "A").Limit(stockSize).Rows()
	for rowsData.Next() {
		_ = db.ScanRows(rowsData, &prodSrv.product)
		itemsList := stock.MagentoItemFormat(prodSrv.product)
		rows = append(rows, itemsList)
	}
	return rows
}

func (stock StockManager) MagentoItemFormat(product Products) map[string]interface{} {
	status := 0
	if product.QtyAvailable > 0 {
		status = 1
	}
	itemsList := make(map[string]interface{})
	itemsList["sku"] = product.Sku
	itemsList["source_code"] = "default"
	itemsList["quantity"] = product.QtyAvailable
	itemsList["status"] = status
	return itemsList
}

func (stock StockManager) MagentoSkuFormat(skus []string) map[string]interface{} {
	itemsList := make(map[string]interface{})
	itemsList["skus"] = skus
	return itemsList
}
