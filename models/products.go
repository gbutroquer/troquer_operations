package models

import (
	"time"
)

type Products struct {
	ID           int       `json:"id,primary_key"`
	Sku          string    `json:"sku" gorm:"size:100;unique"`
	Description  string    `json:"description" gorm:"size:200"`
	QtyInventory int       `json:"qtyInventory"`
	QtyToStock   int       `json:"qtyToStock"`
	QtyAvailable int       `json:"qtyAvailable"`
	Color        string    `json:"color" gorm:"size:50"`
	StatusItem   string    `json:"statusItem" gorm:"size:100"`
	Brand        string    `json:"brand" gorm:"size:100"`
	Model        string    `json:"model" gorm:"size:100"`
	Size         string    `json:"size" gorm:"size:50"`
	Source       string    `json:"source" gorm:"size:10"`
	IsActive     bool      `json:"isActive" gorm:"default:true"`
	SentStock    bool      `json:"sentStock" gorm:"default:false"`
	CreatedAt    time.Time `json:"createdAt" gorm:"autoCreateTime"`
	UpdatedAt    time.Time `json:"updatedAt"`
	DeletedAt    time.Time `json:"deletedAt"`
}
