package models

import (
	"errors"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/spf13/cast"
	"time"
	"troquer.wms.api/helper"
)

type ProductsService struct {
	product Products
}

func (prodSrv ProductsService) Find(page int, totalRecords int) map[string]interface{} {
	var totalRows int64
	var rows []interface{}
	result := make(map[string]interface{})
	db.Model(&Products{}).Where("is_active = ?", true).Count(&totalRows)
	rowsData, _ := db.Model(&prodSrv.product).Where("is_active = ?", true).Limit(totalRecords).Offset(helper.GetOffset(page, totalRecords)).Order("id desc").Rows()
	for rowsData.Next() {
		_ = db.ScanRows(rowsData, &prodSrv.product)
		rows = append(rows, prodSrv.product)
	}
	result["totalRecords"] = totalRows
	result["records"] = rows
	return result
}

func (prodSrv ProductsService) GetBySku(sku string) Products {
	_ = db.Where("sku = ? and is_active = ?", sku, true).First(&prodSrv.product)
	return prodSrv.product
}

func (prodSrv ProductsService) Delete(sku string) (Products Products, err error) {
	productDel := prodSrv.GetBySku(sku)
	if productDel.ID == 0 {
		return productDel, errors.New("the sku doesn't exist")
	}
	today := time.Now()
	productDel.IsActive = false
	productDel.DeletedAt = today
	db.Save(&productDel)
	return productDel, nil
}

func (prodSrv ProductsService) Save(products Products) (product Products, err error) {
	db.Save(&products)
	if products.ID == 0 {
		return products, errors.New("the product has not been created")
	}
	return products, nil
}

func (prodSrv ProductsService) Factory(params map[string]interface{}) Products {
	prodSrv.product.Sku = params["sku"].(string)
	prodSrv.product.Description = params["description"].(string)
	prodSrv.product.QtyInventory = cast.ToInt(params["qtyInventory"])
	prodSrv.product.QtyAvailable = cast.ToInt(params["qtyAvailable"])
	prodSrv.product.QtyToStock = cast.ToInt(params["qtyToStock"])
	prodSrv.product.Color = params["color"].(string)
	prodSrv.product.StatusItem = params["statusItem"].(string)
	prodSrv.product.Brand = params["brand"].(string)
	prodSrv.product.Model = params["model"].(string)
	prodSrv.product.Size = params["size"].(string)
	prodSrv.product.Source = params["source"].(string)
	return prodSrv.product
}

func (prodSrv ProductsService) CheckSkuProp(wms Products, almex Products) (product Products, err error) {
	if !cmp.Equal(almex, wms, cmpopts.IgnoreFields(product, "ID", "Source", "IsActive", "SentStock", "CreatedAt", "UpdatedAt", "DeletedAt")) {
		wms = prodSrv.UpdateFromModel(wms, almex)
		db.Save(&wms)
		return wms, nil
	}
	return wms, nil
}

func (prodSrv ProductsService) UpdateFromModel(old Products, new Products) (product Products) {
	old.Description = new.Description
	old.QtyInventory = new.QtyInventory
	old.QtyToStock = new.QtyToStock
	old.QtyAvailable = new.QtyAvailable
	old.Color = new.Color
	old.StatusItem = new.StatusItem
	old.Brand = new.Brand
	old.Model = new.Model
	old.Size = new.Size
	old.SentStock = false

	return old
}
