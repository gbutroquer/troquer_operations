package models

import (
	"encoding/json"
	"github.com/spf13/cast"
	"os"
	"time"
	"troquer.wms.api/helper"
)

type SyncWmsService struct {
	syncWms SyncWms
}

func (syncSrv SyncWmsService) SyncWmsAlmex(page int, records int, totalPages int, source string) {
	productSrv := ProductsService{}
	stock := StockManager{}
	syncWms := syncSrv.SyncWmsStart(page, totalPages)
	var newProducts int
	if totalPages == 0 {
		wms := (&AlmexWms{}).ProductsInventoryPerPage(page, records, source)
		totalPages = wms.TotalPages
	}
	log := make(map[int]interface{})
	for i := page; i <= totalPages; i++ {
		responseAlmex := (&AlmexWms{}).ProductsInventoryPerPage(i, records, source)
		for _, product := range responseAlmex.ProductsWms {
			exist := productSrv.GetBySku(product.Sku)
			if exist.ID == 0 {
				newProducts++
				_, _ = productSrv.Save(product)
			} else {
				_, _ = productSrv.CheckSkuProp(exist, product)
			}
		}

		list, _ := stock.UpdateStock()
		skuLog, _ := json.Marshal(list)
		log[i] = string(skuLog)
	}
	logDb, _ := json.Marshal(log)
	logText := string(logDb)
	syncSrv.SyncWmsEnd(syncWms, newProducts, totalPages, logText)
}

func (syncSrv SyncWmsService) SyncWmsStart(startPage int, endPage int) (wms SyncWms) {
	params := make(map[string]interface{})
	params["startPage"] = startPage
	params["endPage"] = endPage
	params["totalPage"] = wms.TotalPage
	params["totalProducts"] = 0
	params["success"] = false
	wms = syncSrv.Factory(params)
	db.Save(&wms)
	return wms
}

func (syncSrv SyncWmsService) SyncWmsEnd(wms SyncWms, totalProducts int, pagesWms int, skuLog string) {
	wms.TotalPage = pagesWms
	wms.Success = true
	wms.EndAt = time.Now()
	wms.TotalProducts = totalProducts
	wms.SkuStock = skuLog
	db.Save(wms)
}

func (syncSrv SyncWmsService) Find(page int, totalRecords int) map[string]interface{} {
	var totalRows int64
	var rows []interface{}
	result := make(map[string]interface{})
	db.Model(&SyncWms{}).Count(&totalRows)
	rowsData, _ := db.Model(&syncSrv.syncWms).Limit(totalRecords).Offset(helper.GetOffset(page, totalRecords)).Order("id desc").Rows()
	for rowsData.Next() {
		_ = db.ScanRows(rowsData, &syncSrv.syncWms)
		rows = append(rows, syncSrv.syncWms)
	}
	result["totalRecords"] = totalRows
	result["records"] = rows
	return result
}

func (syncSrv SyncWmsService) Factory(params map[string]interface{}) SyncWms {
	syncSrv.syncWms.StartPage = cast.ToInt(params["startPage"])
	syncSrv.syncWms.EndPage = cast.ToInt(params["endPage"])
	syncSrv.syncWms.TotalPage = cast.ToInt(params["totalPage"])
	syncSrv.syncWms.TotalProducts = cast.ToInt(params["totalProducts"])
	syncSrv.syncWms.Success = cast.ToBool(params["success"])
	return syncSrv.syncWms
}

func (syncSrv SyncWmsService) SyncWmsAlmexCronJob() {
	page := 1
	records := cast.ToInt(os.Getenv("SYNC_RECORDS"))
	source := "cronjob"
	syncSrv.SyncWmsAlmex(page, records, 0, source)
}

func (syncSrv SyncWmsService) GetLastPageAlmexCrobJob() int {
	rowsData, _ := db.Model(&syncSrv.syncWms).Where("success = ?", true).Order("id desc").Limit(1).Rows()
	for rowsData.Next() {
		_ = db.ScanRows(rowsData, &syncSrv.syncWms)
	}
	lastPage := syncSrv.syncWms.EndPage
	if lastPage == 0 {
		lastPage = 1
	}
	return lastPage
}
