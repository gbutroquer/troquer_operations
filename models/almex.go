package models

import (
	"fmt"
	"github.com/spf13/cast"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"strings"
	"troquer.wms.api/helper"
)

type AlmexWms struct {
	User            string
	Password        string
	Account         int
	MessageResponse interface{}
	Row             int
	Page            int
	TotalPages      int
	ProductsWms     map[int]Products
}

func (almex *AlmexWms) ProductsInventoryPerPage(page int, records int, source string) AlmexWms {
	*almex = almex.AlmexConfig()
	request := "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\"><Body><ProductoInventarioPaginacionMethod xmlns=\"http://impl.ws.ck.com/\">"
	request = request + "<Usuario xmlns=\"\">" + cast.ToString(almex.User) + "</Usuario>"
	request = request + "<Password xmlns=\"\">" + cast.ToString(almex.Password) + "</Password>"
	request = request + "<cuenta xmlns=\"\">" + cast.ToString(almex.Account) + "</cuenta>"
	request = request + "<paginaNumero xmlns=\"\">" + cast.ToString(page) + "</paginaNumero>"
	request = request + "<cantidadRegistos xmlns=\"\">" + cast.ToString(records) + "</cantidadRegistos>"
	request = request + "</ProductoInventarioPaginacionMethod></Body></Envelope>"
	payload := strings.NewReader(request)
	response := almex.Send(payload)
	totalPages := helper.ValuesForKey(response, "paginasTotales")
	totalPagesVal := reflect.ValueOf(totalPages).Index(0).Interface()
	almex.TotalPages = cast.ToInt(totalPagesVal)
	productsList := helper.ValuesForKey(response, "productos")
	if len(productsList) > 0 {
		almex.ProductsWms = almex.TransformProducts(productsList, source)
	}
	return *almex
}

func (almex AlmexWms) Send(payload *strings.Reader) map[string]interface{} {
	url := os.Getenv("WMS_ALMEX_API")
	client := &http.Client{
	}
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "text/xml")
	res, err := client.Do(req)
	if res != nil {
		defer func() {
			_ = res.Body.Close()
		}()
		body, _ := ioutil.ReadAll(res.Body)
		soapResponse := string(body)
		doc, _ := helper.DocToMap(soapResponse)
		return doc
	}
	return nil
}

func (almex AlmexWms) AlmexConfig() AlmexWms {
	almex.User = os.Getenv("WMS_ALMEX_USER")
	almex.Password = os.Getenv("WMS_ALMEX_PASSWORD")
	almex.Account = cast.ToInt(os.Getenv("WMS_ALMEX_ACCOUNT"))
	return almex
}

func (almex AlmexWms) TransformProducts(productsList []interface{}, source string) map[int]Products {
	v := reflect.ValueOf(productsList).Index(0).Interface()
	vector := v.([]interface{})
	products := make(map[int]Products)
	for i := range vector {
		productData := make(map[string]interface{})
		element := vector[i]
		row := cast.ToStringMap(element)
		productData["sku"] = row["productoSku"]
		productData["description"] = row["descripcion"]
		productData["qtyInventory"] = row["cantidadInventario"]
		productData["qtyAvailable"] = row["cantidad"]
		productData["qtyToStock"] = row["cantidadSurtir"]
		productData["color"] = row["color"]
		productData["statusItem"] = row["edoCalidad"]
		productData["brand"] = row["marca"]
		productData["model"] = row["modelo"]
		productData["size"] = row["talla"]
		productData["source"] = source
		productIns := ProductsService{}.Factory(productData)
		products[i] = productIns
	}
	return products
}
