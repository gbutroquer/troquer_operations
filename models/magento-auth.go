package models

import (
	"github.com/go-resty/resty/v2"
	"github.com/spf13/cast"
	"github.com/xlzd/gotp"
	"os"
	"strings"
)

type MagentoAuth struct {
}

func (mgAuth MagentoAuth) GetToken() (token string) {
	params := make(map[string]interface{})
	url := os.Getenv("MAGENTO_AUTH")
	params["username"] = os.Getenv("MAGENTO_USERNAME")
	params["password"] = os.Getenv("MAGENTO_PASSWORD")
	params["otp"] = mgAuth.GetOtp()
	client := resty.New()
	resp, _ := client.R().SetBody(params).Post(url)
	token = strings.Trim(resp.String(), "\"")
	return token
}

func (mgAuth MagentoAuth) GetOtp() int {
	key := os.Getenv("OTP_KEY")
	otp := gotp.NewDefaultTOTP(key)
	code := otp.Now()
	return cast.ToInt(code)
}
