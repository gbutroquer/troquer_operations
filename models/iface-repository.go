package models

type Repository interface {
	Find(page int, totalRecords int) map[string]interface{}
}
