# Troquer WMS 
Rest api para realizar la conexión entre diversos provedores de almacenes (wms) con los sistemas internos y portales de ecommerce.
## Requerimientos
* Docker 19.03 o superior
* Golang 1.14.9
* Postgresql 9.6.19
* Git 2.26 o superior
## Instalación
1.Descargar el proyecto del repositorio git
```bash
$ git clone git@github.com:luigiescalante/troquer-wms.git .
```
2.Ejecutar el comando docker compose. 
Para realizar esto nuestro sistema debe de tener docker previamente instalado.
```bash
$ sudo docker-compose up --build -d 
```
En caso de que la base de datos este este en un sistema externo ejecutamos el siguiente comando
```bash
$ sudo docker-compose up --build -d app 
```
3.Validación de los servicios
Para validar que los contenedores docker se inicializaron correctamente revisamos su estatus
```bash
$ sudo docker ps -a
```
## Inicio
Para volver a iniciar los contenedores ejecutamos el siguiente comando.
```bash
$ sudo docker-compose up -d 
```
## Apagar
Para detener los contenedores ejecutamos el siguiente comando.
```bash
$ sudo docker-compose down
```
## Postman
Para poder realizar pruebas con postman se puede descargar la colección del siguiente link:
https://www.getpostman.com/collections/40baa6b1e31a06f103b8