module troquer.wms.api

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/go-co-op/gocron v1.6.2
	github.com/go-oauth2/gin-server v1.0.0
	github.com/go-playground/validator/v10 v10.8.0 // indirect
	github.com/go-resty/resty/v2 v2.6.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/spf13/cast v1.4.0
	github.com/tidwall/buntdb v1.2.4 // indirect
	github.com/tidwall/gjson v1.8.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/ugorji/go v1.2.6 // indirect
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/oauth2.v3 v3.12.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12
)
