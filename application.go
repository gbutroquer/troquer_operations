package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
	ginserver "github.com/go-oauth2/gin-server"
	"github.com/joho/godotenv"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
	"net/http"
	"os"
	"strconv"
	"time"
	"troquer.wms.api/helper"
	"troquer.wms.api/httpd"
	modelsApi "troquer.wms.api/models"
)

func main() {
	_ = godotenv.Load(".env") //Env variables
	//DATABASE PROCESS
	modelsApi.InitDB()
	modelsApi.Migrate()
	//API PROCESS
	manager := manage.NewDefaultManager()
	manager.MustTokenStorage(store.NewMemoryTokenStore()) // token store (memory)
	//manager.MustTokenStorage(store.NewFileTokenStore("db.access")) //token saved in file Recommended for develop and test
	oauth2Id := os.Getenv("OAUTH_ID")
	oauth2Secret := os.Getenv("OAUTH_SECRET")
	oauthDomain := os.Getenv("OAUTH_DOMAIN")
	clientStore := store.NewClientStore()
	_ = clientStore.Set(oauth2Id, &models.Client{
		ID:     oauth2Id,
		Secret: oauth2Secret,
		Domain: oauthDomain,
	})
	manager.MapClientStorage(clientStore)
	ginserver.InitServer(manager) // Initialize the oauth2 service
	ginserver.SetClientInfoHandler(server.ClientFormHandler)

	//CronJob Process
	wmsCronJob := gocron.NewScheduler(time.UTC)
	wmsMinutes, _ := strconv.Atoi(os.Getenv("SYNC_MINUTES"))
	_, _ = wmsCronJob.Every(strconv.Itoa(wmsMinutes) + "m").Do(modelsApi.SyncWmsService{}.SyncWmsAlmexCronJob)
	wmsCronJob.StartAsync()

	router := gin.Default()
	auth := router.Group("/oauth2")
	{
		auth.POST("/token", ginserver.HandleTokenRequest)
	}

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{})
	})
	router.GET("/_healthy", httpd.HealthyCheck)
	//Api routes Api Version V1
	api := router.Group("/v1")
	{
		config := ginserver.Config{
			ErrorHandleFunc: func(c *gin.Context, err error) {
				helper.ErrorAuth(c, err)
			},
		}
		api.Use(ginserver.HandleTokenVerify(config))
		//Products
		api.GET("/products", httpd.GetProducts)
		api.GET("/products/sku/:sku", httpd.GetProductBySku)
		api.POST("/products", httpd.SaveProduct)
		api.DELETE("/products/sku/:sku", httpd.DeleteProduct)
		//Sync Wms
		api.GET("/sync-wms", httpd.SyncWms)
		api.POST("/sync-wms", httpd.GenerateSyncWms)
		//Manage Stock (Magento)
		api.POST("/stock-magento/sku/:sku", httpd.StockManageBySku)
		api.POST("/stock-magento", httpd.StockManage)
	}
	_ = router.Run()
}
