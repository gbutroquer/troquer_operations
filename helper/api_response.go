package helper

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Success(c *gin.Context, data interface{}) {
	var response = gin.H{
		"data": data,
	}
	c.JSON(http.StatusOK, response)
}

func SuccessRows(c *gin.Context, data interface{}, total int) {
	var response = gin.H{
		"data":         data,
		"totalRecords": total,
	}
	c.JSON(http.StatusOK, response)
}

func Error(c *gin.Context, error string) {
	var response = gin.H{
		"error": error,
	}
	c.JSON(http.StatusBadRequest, response)
}

func ErrorAuth(c *gin.Context, err error) {
	var response = gin.H{
		"error": err.Error(),
	}
	c.JSON(http.StatusInternalServerError, response)
	_ = c.AbortWithError(500, err)
}
