package helper

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

func ApiRequest(c *gin.Context) (map[string]interface{}, error) {
	var data map[string]interface{}
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return data, err
	}
	_ = json.Unmarshal(body, &data)
	return data, err
}
